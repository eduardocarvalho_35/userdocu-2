# Electrostatic PDE


## Governing Equations
The governing equation can easily be derived by considering Faraday's law in its stationary form (no time-derivative of the magnetic flux density) and law of Gauss.
Introducing the electric potential $\mathbf{E} = -\nabla{\phi}$, we obatain 
\begin{equation}
-\nabla \cdot {\mathbf{\epsilon}\nabla{\phi}} = 0,
\end{equation}
where $\epsilon$ is the permittivity.
The boundary conditions can be given as
\begin{equation}
\phi = \phi_{\textrm e} \qquad \textrm{on }\Gamma_{\textrm e},
\end{equation}

\begin{equation}
\mathbf{D}\cdot\mathbf{n} = -\mathbf{\epsilon}\nabla{\phi} \cdot\mathbf{n} = q_{\textrm s} \qquad \textrm{on }\Gamma_{\textrm n},
\end{equation}
with $\phi_{\textrm e}$ as the Dirichlet (essential) BC-value, $\Gamma_{\textrm e}$ the essential boundary, $\mathbf{n}$ the normal vector on Neumann (natural) boundary $\Gamma_{\textrm n}$ and $q_{\textrm s}$ the surface charge density.


## Boundary conditions
As described above, there are two types of BC's, Neumann and Dirichlet.
However, since openCFS is physics-based, there are several variations and combinations of these.
They can be defined in the simulation-xml file in the BC-section
```
        <bcsAndLoads>
          <potential name="" value=""/>
          <ground name=""/>
          <charge name="" value=""/>
          <chargeDensity name="" value=""/>
          <fluxDensity name=""/>
          <fieldParallel name=""/>
          <constraint name="Electrodes" quantity="elecPotential"/>
        </bcsAndLoads>
```

* Potential `<potential name="" value=""/>`: Inhomogeneous Dirichlet value $\phi_{\textrm{e}}=\textrm{value}$
* Ground `<ground name=""/>`: Homogeneous Dirichlet value $\phi_{\textrm{e}}=0$
* Charge `<charge name="" value=""/>`: Surface charge $\int_{\textrm{A}}q_s\, ds=\textrm{value}$ (Neumann-type BC), where $ds$ is the infinitesimal surface element
* Charge density `<chargeDensity name="" value=""/>`:  Surface charge density $q_s=\textrm{value}$ (Neumann-type BC)
* Flux density `<fluxDensity name="" value=""/>`: Defines normal component of dielectric displacement $\mathbf{D}\cdot \mathbf{n}=\textrm{value}$ (similar to a surface charge density)
* Field parallel `<fieldParallel name=""/>`: $\mathbf{D}\cdot \mathbf{n}=0$ on the specified surface
* Constraint `<constraint name="Electrodes" quantity="elecPotential"/>` : Puts a constraint on a certain area. This means the specified quantity must be the same for all nodes over the specified area.


## Material
In general, the permittivity is a rank-2 tensor for anisotropic materials, and the 'total' permittivity can be defined via the relative permittivity $\epsilon_{\textrm{r}}$ as
\begin{equation}
\epsilon=\epsilon_{\textrm{0}} \epsilon_{\textrm{r}}.
\end{equation}
It can be specified in the material-xml file e.g. for polyethylene either by defining an insotropic permittivity and/or a tensorial value for the anisotropic version
```
  <material name="polyethylene">
    <electric>
      <permittivity>
	  <linear>
        <isotropic>
           <real> 8.85419E-12 </real>
        </isotropic>
        <tensor dim1="3" dim2="3">
          <real>
            1.99200E-11 0.00000E+00 0.00000E+00
            0.00000E+00 1.99200E-11 0.00000E+00
            0.00000E+00 0.00000E+00 1.99200E-11
          </real>
        </tensor>
		</linear>
      </permittivity>
    </electric>
  </material>
```

## Analysis Types
Since in general we are dealing with a time-independent PDE, we just have a static analysis:

* **Static:** 
```
<analysis>
  <static/>
</static>
```

## Postprocessing results (hysteresis excluded)

* **Electric Potential** (node result, primary solution)
```
<nodeResult type="elecPotential">
```

* **Electric Field Intensity** (element result)
\begin{equation}
\mathbf E = -\nabla \phi
\end{equation}
```
<elemResult type="elecFieldIntensity">
```

* **Electric Energy Density** (element result)
\begin{equation}
e =  \epsilon \mathbf{E}  \cdot \mathbf{E}
\end{equation}
```
<elemResult type="elecEnergyDensity">
```

* ** Charge Density**  (element result)
\begin{equation}
q_{{s}}
\end{equation}
```
<elemResult type="elecChargeDensity">
```


<!---### Surface Region Results --->
* **Electric Charge** (surface region result)
\begin{equation}
Q = \int_{{A}} q_{{s}} ds
\end{equation}
```
<surfRegionResult type="elecCharge">
```


<!--- ### Region Results --->
* **Electric Energy** (region result)
\begin{equation}
E = \int_{{\Omega}}  \mathbf{E}  \cdot \mathbf{E} \, dV
\end{equation}
```
<regionResult type="elecEnergy">
```

## Modelling assumptions
* Stationary fieldst ($\frac{\partial \mathbf{B}}{\partial t} \approx 0$ and $\frac{\partial \mathbf{D}}{\partial t} \approx 0$)
* Non-conductive materials, such as electric isolators (free charge: $q_e=0$)
* Linear constitutive relation ($ \mathbf{D}=\mathbf{\epsilon}\mathbf{E}






