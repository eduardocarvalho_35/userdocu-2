Coupled Mechanic-Acoustic Eigenvalues in a Fluid Filled Pipe
============================================================

All the files in this tutorial can be [`downloaded here`](CoupledEigenValues.zip).

We consider a fluid filled pipe in an axi-symmetric setting.
Three eigenvalue analyses are done, first for the individual
problems (fluid only, pipe only) and then for the coupled case.

1. Look at the geometry definition in the Trelis journal file [`pipe.jou`](pipe.jou)
2. Go through the simulation input [`mech-acou-eigenvalues.xml`](mech-acou-eigenvalues.xml). The material file [`mat.xml`](mat.xml) is pretty standard
3. Run the simulation using the run-script [`run.sh`](run.sh)
4. Look at the mode shapes for the different sequence steps. You can use the provided paraview state file [`coupled-visualization.pvsm`](coupled-visualization.pvsm)

Things to think about
---------------------

* How could you estimate the first fluid-only mode analytically?
* What does the mode shape and natural frequency of the pipe only represent? 
  What do the natural frequencies depend on?
* Why are the natural frequencies of the coupled system lower?
* Can you estimate the wave speed of a compression wave in the pipe?
