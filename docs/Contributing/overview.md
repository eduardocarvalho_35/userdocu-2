Contributing to the openCFS userdocu
====================================

We're happy about contribuions to keep the documentation growing and up to date!
Below you find some details on how to do that.
If you have any questions, open an [issue](https://gitlab.com/openCFS/userdocu/-/issues).

To get started editing the documentation (locally on your machine) the following steps are required

1. clone/fork the [userdocu source repository](https://gitlab.com/openCFS/userdocu).
2. install [MkDocs](https://www.mkdocs.org/) (The mkdocs style is based on markdown language and super easy to edit).
3. edit in the source, view the result locally.
4. commit your changes to your forked repository, and push it to the gitlab server.
5. create a merge request on gitlab, assign it to a project maintainer for review.

In the following the steps are explained in more detail.

**Please keep in mind, that this will be the documentation for users and not developers**
