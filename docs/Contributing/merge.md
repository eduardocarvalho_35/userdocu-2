# Create a merge Request
In order to merge your repository into the original repository, your branch/forked repository have to be merged into the original repository.

## Im finished with editing. What should i do?
Great. Now you have to do following steps:

* [Commit and push everything to your repository](#updating-your-local-repository)
* [Create a merge request](#create-merge-request)
* [Mark it as ready](#mark the mergerequest as ready)

### Updating your local repository
Im just running quickly over some basics. For more details look into [git-slides](slides_git.pdf).

* With `# git status` you can see the current status of your repostitory (branchname, filechanges etc.). Here you can see if your local repostiory is up to date.
* With `# git add file` you can add a change/file to your repository
* With `# git commit` you commit these adds to the repostiory. Note: Now you have to write a short commit message e.g. "Added XYZ".
* With `# git push` you push your local repository to gitlab.

## Create Merge Request

Once you pushed your branch onto gitlab and it passes the pipeline, its time to create your merge request.
### Follow the steps in the gif below:
* Click `merge request` on the sidebar
* Click `new merge request`
* Select source branche: This is your forked repo e.g `your_fork/userdocu : master`
* Select target branche: This is `openCFS/userdocu : master` (the branch your fork is based on) 
* Click `Compare cranches and continue`
* Add a meaningful title and description
* Create merge request
* Hint: assign yourself as assignee, so you find the merge request on gitlab under `Merge requests`.

![merge-gif](merge.gif)

## Mark the mergerequest as ready
Once:

* the merge request is created
* you are done with all the work 
* merge request passes the pipeline (automatic tests to garantuee you do not blow something up)

you can mark the merge request as ready. Click on `Mark as ready`.

## Assign a reviewer

Once your merge-request is marked as ready, you should assign a developer as reviewer (@apapst, @kroppert, etc..), so we get notified a new merge-request is ready to review.
If the merge request gets approved, your branche will get merged into the master and deployed on the userdocumentation website.
![AssignReviewer](reviewer.jpg)

If you are not able to assign a developer, contact them via email.
