Installing ParaVew and the CFSReader Plugin
===========================================

In order to read openCFS native HDF5 data format (`*.cfs` files) with paraview you need our **CFSReader plugin**.

ParaView
--------

You can build ParaView from source or install an [official ParaView release](https://www.paraview.org/download/) for your operation system.

CFSReader Plugin
----------------

The CFSReader plugin must be built for a matching ParaView version.
Look into our [ParaView repository](https://gitlab.com/openCFS/ParaView) for details.#

We provide the following pre-built plugins:

* **CFSReader v20.03 for ParaView 5.9.1** on [Linux](https://owncloud.tuwien.ac.at/index.php/s/ocVr4a2RhhgGfem/download) (works for ParaView 5.9.0 too)
* for older versions see the [legacy docs](https://cfs-doc.mdmt.tuwien.ac.at/mediawiki/index.php/Installation)

The plugin consits of several shared libraries which must be placed such that ParaView can find them.
Just extract the archive into the directory of your ParaView installation:, e.g. on Linux
```shell
tar -xzvf CFSReader-v21.03_Linux_ParaView-v5.9.0.tar.gz -C <install-dir>
```
The plugin should end up in `lib/paraview-5.9/plugins/CFSReader`, from where ParaView should automatically load it (restart required). 

In order verify if the plugin was loaded in ParaView open ParaView's *Plungin Manager* from *Tools -> Manage Plugins ...* and look for the *CFSReader* plugin.
To manually load it choose *load new* and select the plugin file (e.g. on linux `CFSReader.so`).
