# Result Explanation

There are various result-types in openCFS, which can be specified in the xml-file (node-, element-, region-, surfaceElement-, surfaceRegion-results):
```
<storeResults>
    <elemResult type="">
        <allRegions/>
    </elemResult>
    <nodeResult type="">
        <allRegions/>
    </nodeResult>
    <regionResult type="">
        <allRegions/>
    </regionResult>
    <regionResult type="">
        <allRegions/>
    </regionResult>
    <surfElemResult type="">
        <allRegions/>
    </surfElemResult>
    <surfRegionResult type="">
        <allRegions/>
    </surfRegionResult>
</storeResults>
```

The type of your desired quantity depends on the type of the FE-result:

* A **primary** result corresponds to the degrees of freedom in the FE system. For example: The temperature $T$ in the heat conduction PDE is a primary result. The output is either on nodes (for nodal elements) or on elements (for edge elements).
* A **derived** result is computed from the primary by applying a certain operator, e.g., gradient, curl, divergence. For example: The heat flux density is the gradient of the temperature $-\nabla T$ in the heat conduction PDE.
The output is on elements (for nodal as well as edge elements)
* An **integrated** result is computed by integrating a result over a volume. For example, integrating the heat flux density over a volume, results in the total heat flux.
The output is stored in .hist files in the history-folder.




