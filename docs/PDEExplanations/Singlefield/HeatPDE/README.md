# Heat Conduction PDE


## Governing Equations
The governing equation can be derived by considering Fourier's law and the first law of thermodynamics.
Mathematically this PDE is equivalent to the electric flow PDE, both of them are Poisson problems.

The heat conduction problem can be stated as:

\begin{equation}
\rho c_{\textrm{m}} \frac{\partial T}{\partial t} -\nabla \cdot ({k\nabla{T}}) = \dot{{q}},
\end{equation}
where $\rho$ is the mass density, $c_{\textrm{m}}$ the volumetric heat capacity, $k$ the thermal conductivity and $\dot{q}$ the heat flux density.


## Boundary conditions
As described above, there are two types of BC's, Neumann and Dirichlet.
However, since openCFS is physics-based, there are several variations and combinations of these.
They can be defined in the simulation-xml file in the BC-section
```
        <bcsAndLoads>
            <temperature name="" value=""/>
            <heatFlux name="" value=""/>
            <heatSource name="" value=""/>
            <heatSourceDensity>
<!--                 via: -->
<!--                    * coupled to another PDE -->
<!--                    * input from external simulation -->
<!--                    * scattered data input from csv file -->
<!--                    * sequence step (forward coupling from another PDE) -->
            </heatSourceDensity>
            <heatTransport name=""  volumeRegion="" heatTransferCoefficient="" bulkTemperature="20.0"/>
            <periodic slave="" master="" quantity=""/>
            <elecPowerDensity>
<!--                 via: -->
<!--                    * coupling to other PDE -->
            </elecPowerDensity>
        </bcsAndLoads>
```

* Temperature `<temperatur name="" value=""/>`: Inhomogeneous Dirichlet value $T_{\textrm{e}}$
* Heat Flux `<heatFlux name="" value=""/>`: Prescribes the heat flux across a certain boundary $\dot{\mathbf{q}}\cdot \mathbf{n}$
* Heat Source Density `<heatSourceDensity/>`: Prescribes the volumetric heat source denities $\dot{q}$
* Heat Transport `<heatTransport name=""  volumeRegion="" heatTransferCoefficient="" bulkTemperature="20.0"/>`: Describes the heat transport between two media, e.g. iron-air; *name* defines the surface through which the heat is transported, *volumeRegion* defines the positive direction for the heat flux, e.g. we define the direcion iron->air as positive, then the *volumeRegion* has to be the iron-region. *heatTransferCoefficient* defines the transfer coefficient and *bulkTemperature* defines the ambient temperature of, e.g., the surrounding air domain
* Electric Power Density `<elecPowerDensity>`: Defines the electric power density as a volume source from a coupled computation as a right-hand-side quantity ($\dot{q}$)


##Material

The material parameters $\rho$, $c_{\textrm{m}}$ and $k$ are defined in the material-xml file, e.g. for iron

```
  <material name="iron">
    <heatConduction>
      <density>
        <linear>
          <real> 7870 </real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real>  444 </real>
        </linear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real> 80.4 </real>
          </isotropic>
        </linear>
      </heatConductivity>
    </heatConduction>
  </material>
```


## Analysis Types
Since in general we are dealing with a time-dependent PDE, we can distinguish three analysis types:

* Static: $\frac{\partial T}{\partial t}=0$, in this case, the PDE is a purely elliptic PDE (Poisson problem)
```
    <analysis>
       <static/>
    </analysis>
```
* Transient: $\frac{\partial T}{\partial t}\neq 0$

```
    <analysis>
      <transient>
        <numSteps>10</numSteps>
        <deltaT>0.1</deltaT>
      </transient>
    </analysis>
```

* **Harmonic:**  $\partial / \partial t (\cdot) = j \omega (\cdot)$
```
<analysis>
	<harmonic>
		<numFreq>10</numFreq>
		<startFreq>10</startFreq>
		<stopFreq>3000</stopFreq>
		<sampling>linear</sampling>
	</harmonic>
</analysis>
```



## Postprocessing Results

#### Node Results
* **Temperature** (primary solution)
```
<nodeResult type="heatTemperature">
```

* **Temperature Time-Derivative**
```
<nodeResult type="heatTemperatureD1">
```


#### Element Results
* **Heat Flux Density** 
\begin{equation}
\dot{\mathbf q} = -k \nabla T
\end{equation}
```
<elemResult type="heatFluxDensity">
```


#### Surface Element Results
* **Heat Flux Density Surface Normal** 
\begin{equation}
\dot{q}_{\textrm{n}} = \dot{\mathbf q} \cdot \mathbf n
\end{equation}
```
<surfElemeResult type="heatFluxIntensity">
```


#### Surface Region Results
* **Heat Flux** 
\begin{equation}
\dot{Q} = \int_{{A}} \dot{q}_{\textrm{n}} \, ds
\end{equation}
```
<surfRegionResult type="heatFlux">
```


