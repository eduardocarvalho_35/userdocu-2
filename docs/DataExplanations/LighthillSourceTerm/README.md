# Lighthill Source Term

The Lighthill source term filter computes aeroacoustic source terms based on Lighthills analogy for incompressible flows. Therefore, first the Lighthill source term vector is computed

\begin{equation}
\nabla \cdot [T] = \nabla(\frac{1}{2} u \cdot u ) + L \, ,
\end{equation}

with $u$ as velocity, and $L$ as Lamb vector, and $[T]$ as the Lighthill stress tensor. Finally, as actual source term the divergence of the source term vector is computed and establised as outputQuantity. 

\begin{equation}
\nabla \cdot \nabla \cdot [T] = \nabla \cdot (\nabla(\frac{1}{2} u \cdot u ) + L)
\end{equation}

It is possible to compute the Lighthill source term only based on the velocity, or on the velocity and the vorticity. If only the velocity is defined
the vorticity is computed internally. The density tag is not implemented all the way through and therefore curruntly not working!

* The epsilonScaling Parameter scales the radial basis functions used for computing spatial derivatives. It controlls the "smoothness" of the basis function. The smoother the gauss-like surface is, the better the results will be BUT only until a certain number, when the matrix becomes to ill-contioned, which will result in very bada results. Typical values: 1e-1 - 1e-4.
* The kScaling parameter is an optional parameter and defines a constant term that is added to the radial basic function.
* The betaScaling parameter defines the slope of a linear term that is added to the radial basis function.
* The logEps Parameter enables a detailes console output (minimal distance, maximal distance, optimized parameters). Therefore, it should only be used if the spatial derivatives are investigated, because it totally spams the console

```

    <aeroacoustic type="AeroacousticSource_LighthillSourceTerm" inputFilterIds="..." id="...">
      <RBF_Settings  epsilonScaling="1e-4"  kScaling="" betaScaling="" logEps=false/>
      <scheme/>
      <sourceSum>true</sourceSum>
      <targetMesh>
        <hdf5 fileName="..."/>
      </targetMesh>
      <ResultList>
        <velocity resultName="..."/>
        <vorticity/>
        <density/>   //NOT WORKING
        <outputQuantity resultName="..."/>
      </ResultList>
      <regions>
        <sourceRegions>
          <region name="..."/>
        </sourceRegions>
        <targetRegions>
          <region name="..."/>
        </targetRegions>
      </regions>
    </aeroacoustic>
    
```

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [FE-based Interpolation]._
