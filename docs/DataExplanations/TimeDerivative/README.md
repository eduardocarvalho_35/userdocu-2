# Time Derivative (Simplified PCWE Source Term)

For low flow velocities convective effects can be neglected for the PCWE ([see Aeroacoustic Simulations](../PDEExplanations/Singlefield/AcousticPDE/)), and the resulting source term simplifies to just the time derivative of the incompressible pressure $\partial p^{\mathrm{ic}}/\partial t$ [@schoder2019b]. This is the reason why the time derivative filter is placed in the section *Aeroacoustic Source Terms*. Of course, the filter can be applied to a different quanitity, of which a time derivative is required, as well.

The time derivative of the desired quantitiy $\dot{q}(t)$ is computed by a smooth noise-robust differentiator, which supresses high frequencies and is precise on low frequencies, according to [Holoborodko](http://www.holoborodko.com/pavel/numerical-methods/numerical-derivative/smooth-low-noise-differentiators/). For an efficient and robust calculation the order of the differentiator is set to $N=5$ and the time derivative is calculated by 
\begin{equation}
\dot{q}(t)=\frac{2(q_1-q_{-1})+q_2-q_{-2}}{8 \Delta t},
\end{equation}
where the index of $q$ defines the time step relative to the time step of which the derivative is calculated and $\Delta t$ is the time step size. The computation only requires the definition of the input quantity (*inputQuantity*-tag) and the desired name of the output quantity (*outputQuantity*-tag) as indicated below.

```
<timeDeriv1 id="TimeDerivative" inputFilterIds="input">
	<singleResult>
		<inputQuantity resultName="fluidMechPressure"/>
		<outputQuantity resultName="acouRhsLoadP"/>
	</singleResult>
</timeDeriv1>
```

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [Time Derivative]._

# References
\bibliography

